package com.mbadziong.stooq.stooq.data.marketindex;

import com.mbadziong.stooq.stooq.data.exception.CsvFormatException;
import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import com.mbadziong.stooq.stooq.data.parser.StooqCsvParser;
import com.mbadziong.stooq.testdata.SampleCsv;
import com.mbadziong.stooq.testdata.SampleMarketIndexesList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * Created by Aneta on 19.09.2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class StooqMarketIndexDownloaderTest {

    @InjectMocks
    private StooqMarketIndexDownloader downloader;

    @Mock
    private StooqCsvParser stooqCsvParser;

    @Mock
    RestTemplate restTemplate;

    @Before
    public void setUp() throws Exception {
        when(restTemplate.getForObject(any(String.class), eq(String.class))).thenReturn(SampleCsv.VALID_CSV);
        when(stooqCsvParser.getMarketIndex(SampleCsv.VALID_CSV)).thenReturn(SampleMarketIndexesList.wigMarketIndex);
    }

    @Test
    public void testCurrentWigValue() {
        MarketIndex wigMarketIndex = downloader.getCurrentMarketIndex("wig");

        assertEquals(SampleMarketIndexesList.wigMarketIndex.getIndexValue(), wigMarketIndex.getIndexValue());
    }

    @Test
    public void testGetCurrentValue_returnLatestValueIfParserError() throws CsvFormatException {
        MarketIndex wigMarketIndex = downloader.getCurrentMarketIndex("wig");

        assertEquals(SampleMarketIndexesList.wigMarketIndex.getIndexValue(), wigMarketIndex.getIndexValue());

        when(stooqCsvParser.getMarketIndex(SampleCsv.VALID_CSV)).thenThrow(CsvFormatException.class);

        wigMarketIndex = downloader.getCurrentMarketIndex("wig");

        assertEquals(SampleMarketIndexesList.wigMarketIndex.getIndexValue(), wigMarketIndex.getIndexValue());
    }

    @Test
    public void testGetCurrentValue_returnLatestValueIfHttpClientError() {
        MarketIndex wigMarketIndex = downloader.getCurrentMarketIndex("wig");

        assertEquals(SampleMarketIndexesList.wigMarketIndex.getIndexValue(), wigMarketIndex.getIndexValue());

        when(restTemplate.getForObject(any(String.class), eq(String.class))).thenThrow(RestClientException.class);

        wigMarketIndex = downloader.getCurrentMarketIndex("wig");

        assertEquals(SampleMarketIndexesList.wigMarketIndex.getIndexValue(), wigMarketIndex.getIndexValue());
    }
}