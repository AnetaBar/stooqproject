package com.mbadziong.stooq.stooq.data.parser;

import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import com.mbadziong.stooq.testdata.SampleCsv;
import com.mbadziong.stooq.stooq.data.exception.CsvFormatException;
import com.mbadziong.stooq.testdata.SampleMarketIndexesList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StooqCsvParserTest {

    @Autowired
    private StooqCsvParser stooqCsvParser;

    @Test
    public void testGetMarketIndexValue_ValidCsv() throws Exception {
        MarketIndex marketIndex = stooqCsvParser.getMarketIndex(SampleCsv.VALID_CSV);

        assertEquals(SampleMarketIndexesList.wigMarketIndex.getIndexValue(), marketIndex.getIndexValue());
        assertEquals(SampleMarketIndexesList.wigMarketIndex.getIndexName(), marketIndex.getIndexName());
        assertEquals(SampleMarketIndexesList.wigMarketIndex.getIndexTime(), marketIndex.getIndexTime());
    }

    @Test(expected = CsvFormatException.class)
    public void testGetMarketIndexValue_InvalidCsv() throws CsvFormatException {
        stooqCsvParser.getMarketIndex(SampleCsv.INVALID_CSV);
    }

    @Test(expected = CsvFormatException.class)
    public void testGetMarketIndexValue_EmptyCsv() throws CsvFormatException {
        stooqCsvParser.getMarketIndex(SampleCsv.EMPTY_CSV);
    }
}
