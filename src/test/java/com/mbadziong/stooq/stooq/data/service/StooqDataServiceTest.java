package com.mbadziong.stooq.stooq.data.service;

import com.mbadziong.stooq.stooq.data.marketindex.*;
import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import com.mbadziong.stooq.testdata.SampleMarketIndexesList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StooqDataServiceTest {

    @InjectMocks
    private StooqDataService stooqDataService;

    @Mock
    private StooqMarketIndexDownloader downloader;

    @Before
    public void setUp() throws Exception {
        when(downloader.getCurrentMarketIndex("wig")).thenReturn(SampleMarketIndexesList.wigMarketIndex);
        when(downloader.getCurrentMarketIndex("wig20")).thenReturn(SampleMarketIndexesList.wig20MarketIndex);
        when(downloader.getCurrentMarketIndex("fw20")).thenReturn(SampleMarketIndexesList.fw20MarketIndex);
        when(downloader.getCurrentMarketIndex("mwig40")).thenReturn(SampleMarketIndexesList.mwig40MarketIndex);
        when(downloader.getCurrentMarketIndex("swig80")).thenReturn(SampleMarketIndexesList.swig80MarketIndex);
    }

    @Test
    public void testGetAll() throws Exception {
        List<MarketIndex> current = stooqDataService.getAllMarketIndexes();

        assertEquals(SampleMarketIndexesList.getSampleMarketReadingList(), current);
    }
}