package com.mbadziong.stooq.stooq.report.service;

import com.mbadziong.stooq.stooq.report.ReportWriter;
import com.mbadziong.stooq.testdata.SampleMarketIndexesList;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReportServiceTest {

    @InjectMocks
    private ReportService reportService;

    @Mock
    private ReportWriter reportWriter;

    @Test
    public void testHandleNewMarketIndex_validMarketIndex() throws Exception {
        reportService.handleNewMarketReading(SampleMarketIndexesList.getSampleMarketReadingList());

        verify(reportWriter, times(1)).processAndWriteToFile(SampleMarketIndexesList.getSampleMarketReadingList());
    }

    @Test
    public void testHandleNewMarketIndex_invalidMarketIndex() throws Exception {
        reportService.handleNewMarketReading(SampleMarketIndexesList.getSampleMarketReadingListWithNull());

        verify(reportWriter, times(0)).processAndWriteToFile(SampleMarketIndexesList.getSampleMarketReadingList());
    }

    @Test
    public void testHandleNewMarketIndex_twoTimesTheSameMarketIndex() throws Exception {
        reportService.handleNewMarketReading(SampleMarketIndexesList.getSampleMarketReadingList());
        reportService.handleNewMarketReading(SampleMarketIndexesList.getSampleMarketReadingList());

        verify(reportWriter, times(1)).processAndWriteToFile(SampleMarketIndexesList.getSampleMarketReadingList());
    }


}