package com.mbadziong.stooq.testdata;

import com.mbadziong.stooq.stooq.data.model.MarketIndex;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Aneta on 19.09.2018.
 */
public class SampleMarketIndexesList {
    private static final DateTimeFormatter dateTimePattern = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    public static final MarketIndex wigMarketIndex = new MarketIndex("WIG", BigDecimal.valueOf(59406.36), LocalDateTime.parse("2017-04-06T17:15:00", dateTimePattern));
    public static final MarketIndex wig20MarketIndex = new MarketIndex("WIG20", BigDecimal.valueOf(2.22), LocalDateTime.parse("2017-04-06T13:18:01", dateTimePattern));
    public static final MarketIndex fw20MarketIndex = new MarketIndex("FW20", BigDecimal.valueOf(3.33), LocalDateTime.parse("2017-04-06T16:18:01", dateTimePattern));
    public static final MarketIndex mwig40MarketIndex = new MarketIndex("MWIG40", BigDecimal.valueOf(4.44), LocalDateTime.parse("2017-04-06T13:18:01", dateTimePattern));
    public static final MarketIndex swig80MarketIndex = new MarketIndex("SWIG80", BigDecimal.valueOf(5.55), LocalDateTime.parse("2017-04-06T11:18:01", dateTimePattern));

    private static List<MarketIndex> sampleMarketIndexesList = new ArrayList<>();
    private static List<MarketIndex> sampleMarketIndexesListWithNull = new ArrayList<>();

    public static List<MarketIndex> getSampleMarketReadingList() {
        Collections.addAll(sampleMarketIndexesList, wigMarketIndex, wig20MarketIndex,
                fw20MarketIndex, mwig40MarketIndex, swig80MarketIndex);
        return sampleMarketIndexesList;
    }

    public static List<MarketIndex> getSampleMarketReadingListWithNull() {
        Collections.addAll(sampleMarketIndexesListWithNull, wigMarketIndex,
                wig20MarketIndex, fw20MarketIndex, mwig40MarketIndex, null);
        return sampleMarketIndexesListWithNull;
    }
}


