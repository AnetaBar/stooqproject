(function () {
    'use strict';

    require('chart.js');

    function StooqHomeController($rootScope, $scope, $http, stooqData, stooqPlot) {

        $scope.ctx = document.getElementById("canvas").getContext("2d");

        $scope.lineChartData = stooqPlot.defaultDataConfig();

        $scope.myLine = Chart.Line($scope.ctx, {
            data: $scope.lineChartData,
            options: stooqPlot.defaultPlotOptions()
        });

        $http({
            method: 'GET',
            url: '/report.json'
        }).then(function successCallback(response) {
            var stooqDataRows = response.data;
            for (var rowNum = 0; rowNum < stooqDataRows.length; rowNum++) {
                var currentRow = stooqDataRows[rowNum];
                $scope.loadNewStooqDataRow(currentRow.marketIndexesList);
            }
            $scope.myLine.update();
        }, function errorCallback(response) {
            //nothing to init
        });

        $scope.loadNewStooqDataRow = function (stooqData) {
            var datasets = $scope.lineChartData.datasets;

            stooqData.forEach(marketReading => {
                var dataset = datasets.find(item => item.label === marketReading.indexName);
            var singleIndex = {x: toDate(marketReading.indexTime), y: marketReading.indexValue};
            datasets[datasets.indexOf(dataset)].data.push(singleIndex);
        })
        };

        $rootScope.$on('stooq-data-changed', function (event, stooqData) {
            $scope.loadNewStooqDataRow(stooqData);
            $scope.myLine.update();
        });

        function toDate(d) {
            return new Date(
                d[0], //year
                d[1] -1, //months from 0
                d[2], //day
                d[3], //hour
                d[4], //minute
                d[5] //second
            );
        };
    }

    module.exports = StooqHomeController;
}());
