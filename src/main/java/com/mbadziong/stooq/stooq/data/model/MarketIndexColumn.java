package com.mbadziong.stooq.stooq.data.model;

/**
 * Created by Aneta on 19.09.2018.
 */
public enum MarketIndexColumn {

    MARKET_INDEX_NAME_COLUMN("Symbol"), MARKET_INDEX_TIME_COLUMN("Czas"), MARKET_INDEX_DATE_COLUMN("Data"), MARKET_INDEX_VALUE_COLUMN("Zamkniecie");

    String columnName;

    MarketIndexColumn(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return columnName;
    }
}
