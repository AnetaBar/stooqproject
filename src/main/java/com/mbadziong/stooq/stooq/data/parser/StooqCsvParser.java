package com.mbadziong.stooq.stooq.data.parser;

import com.mbadziong.stooq.stooq.data.exception.CsvFormatException;
import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import com.mbadziong.stooq.stooq.data.model.MarketIndexColumn;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class StooqCsvParser {

    private static final int STOOQ_CSV_RESPONSE_ROWS = 2;
    private static final String STOOQ_CSV_EOL = "\r\n";
    private static final String STOOQ_CSV_DELIMITER = ",";
    private static final String dateTimePattern = "yyyy-MM-dd'T'HH:mm:ss";

    public MarketIndex getMarketIndex(String csv) throws CsvFormatException {
        Map<String, String> stooqMap = parseCsv(csv);
        MarketIndex marketIndex = buildMarketIndex(stooqMap);
        return marketIndex;
    }

    private MarketIndex buildMarketIndex(Map<String, String> stooqMap) {
        MarketIndex marketIndex = new MarketIndex();
        marketIndex.setIndexName(stooqMap.get(MarketIndexColumn.MARKET_INDEX_NAME_COLUMN.getColumnName()));
        marketIndex.setIndexValue(new BigDecimal(stooqMap.get(MarketIndexColumn.MARKET_INDEX_VALUE_COLUMN.getColumnName())));

        String date = stooqMap.get(MarketIndexColumn.MARKET_INDEX_DATE_COLUMN.getColumnName());
        String time = stooqMap.get(MarketIndexColumn.MARKET_INDEX_TIME_COLUMN.getColumnName());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dateTimePattern);
        StringBuilder dateTime = new StringBuilder().append(date).append("T").append(time);
        LocalDateTime parsedDateTime = LocalDateTime.parse(dateTime, formatter);

        marketIndex.setIndexTime(parsedDateTime);
        return marketIndex;
    }

    private Map<String, String> parseCsv(String csv) throws CsvFormatException {
        List<String> csvRows = getCsvRows(csv);

        List<String> columnNames = new ArrayList<>(Arrays.asList(csvRows.get(0).split(STOOQ_CSV_DELIMITER)));
        List<String> columnValues = new ArrayList<>(Arrays.asList(csvRows.get(1).split(STOOQ_CSV_DELIMITER)));

        if (columnNames.size() != columnValues.size()) {
            throw new CsvFormatException(
                    String.format("Not valid csv. Names row length different than values row length. %d != %d",
                            columnNames.size(),
                            columnValues.size()
                    )
            );
        }

        Map<String, String> stooqMap = IntStream.range(0, columnNames.size())
                .boxed()
                .collect(Collectors.toMap(columnNames::get, columnValues::get));

        return stooqMap;
    }

    List<String> getCsvRows(String csv) throws CsvFormatException {
        List<String> csvRows = new ArrayList<>();

        if(csv != null) {
            csvRows = Pattern.compile(STOOQ_CSV_EOL).splitAsStream(csv).collect(Collectors.toList());
        }

        if(csvRows.size() != STOOQ_CSV_RESPONSE_ROWS) {
            throw new CsvFormatException(
                    String.format("Not valid stooq csv response. Wanted %s rows, received %s",
                            STOOQ_CSV_RESPONSE_ROWS,
                            csvRows.size())
            );
        }
        return csvRows;
    }

}