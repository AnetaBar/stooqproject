package com.mbadziong.stooq.stooq.data.marketindex;


import com.mbadziong.stooq.stooq.data.exception.CsvFormatException;
import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import com.mbadziong.stooq.stooq.data.parser.StooqCsvParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

@Component
public class StooqMarketIndexDownloader {

    private static final Logger LOGGER = LoggerFactory.getLogger(StooqMarketIndexDownloader.class);

    protected final static String STOOQ_URL = "https://stooq.pl/q/l/?s=%s&f=sd2t2ohlc&h&e=csv";

    protected RestTemplate restTemplate;

    private StooqCsvParser stooqCsvParser;

    private MarketIndex latestIndex;

    public StooqMarketIndexDownloader(RestTemplate restTemplate, StooqCsvParser stooqCsvParser) {
        this.restTemplate = restTemplate;
        this.stooqCsvParser = stooqCsvParser;
    }

    public MarketIndex getCurrentMarketIndex(String stockMarketIndexName) {
        MarketIndex index = latestIndex;
        try {
            String response = restTemplate.getForObject(
                    String.format(STOOQ_URL, stockMarketIndexName),
                    String.class);

            index = stooqCsvParser.getMarketIndex(response);
            latestIndex = index;
        } catch (CsvFormatException e) {
            LOGGER.error("Error during csv parse, returning latest value instead.", e);
        } catch (RestClientException e) {
            LOGGER.error("Error during getting csv, returning latest value instead.", e);
        }
        return index;
    }
}
