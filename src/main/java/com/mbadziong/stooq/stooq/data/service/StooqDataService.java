package com.mbadziong.stooq.stooq.data.service;

import com.mbadziong.stooq.stooq.data.marketindex.*;
import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import com.mbadziong.stooq.stooq.data.model.StooqMarketIndex;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class StooqDataService {

    private StooqMarketIndexDownloader stooqMarketIndexDownloader;

    public StooqDataService(StooqMarketIndexDownloader stooqMarketIndexDownloader) {
        this.stooqMarketIndexDownloader = stooqMarketIndexDownloader;
    }

    public List<MarketIndex> getAllMarketIndexes() {
        List<MarketIndex> result = new ArrayList<>();
        MarketIndex wigMarketIndex = stooqMarketIndexDownloader.getCurrentMarketIndex(StooqMarketIndex.WIG.stockMarketIndex());
        MarketIndex wig20MarketIndex = stooqMarketIndexDownloader.getCurrentMarketIndex(StooqMarketIndex.WIG20.stockMarketIndex());
        MarketIndex wig20FutMarketIndex = stooqMarketIndexDownloader.getCurrentMarketIndex(StooqMarketIndex.WIG20FUT.stockMarketIndex());
        MarketIndex mwig40MarketIndex = stooqMarketIndexDownloader.getCurrentMarketIndex(StooqMarketIndex.MWIG40.stockMarketIndex());
        MarketIndex swig80MarketIndex = stooqMarketIndexDownloader.getCurrentMarketIndex(StooqMarketIndex.SWIG80.stockMarketIndex());
        Collections.addAll(result, wigMarketIndex, wig20MarketIndex,
                wig20FutMarketIndex, mwig40MarketIndex, swig80MarketIndex);


        return result;
    }

}
