package com.mbadziong.stooq.stooq.data.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class MarketIndex {
    private String indexName;
    private BigDecimal indexValue;
    private LocalDateTime indexTime;

    public MarketIndex(String indexName, BigDecimal indexValue, LocalDateTime indexTime) {

        this.indexName = indexName;
        this.indexValue = indexValue;
        this.indexTime = indexTime;
    }

    public MarketIndex() {
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public BigDecimal getIndexValue() {
        return indexValue;
    }

    public void setIndexValue(BigDecimal indexValue) {
        this.indexValue = indexValue;
    }

    public LocalDateTime getIndexTime() {
        return indexTime;
    }

    public void setIndexTime(LocalDateTime indexTime) {
        this.indexTime = indexTime;
    }

    @Override
    public String toString() {
        return "MarketIndex{" +
                "indexName='" + indexName + '\'' +
                ", indexValue=" + indexValue +
                ", indexTime=" + indexTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MarketIndex that = (MarketIndex) o;

        if (indexName != null ? !indexName.equals(that.indexName) : that.indexName != null) return false;
        if (indexValue != null ? !indexValue.equals(that.indexValue) : that.indexValue != null) return false;
        return indexTime != null ? indexTime.equals(that.indexTime) : that.indexTime == null;
    }

    @Override
    public int hashCode() {
        int result = indexName != null ? indexName.hashCode() : 0;
        result = 31 * result + (indexValue != null ? indexValue.hashCode() : 0);
        result = 31 * result + (indexTime != null ? indexTime.hashCode() : 0);
        return result;
    }
}
