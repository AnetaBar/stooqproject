package com.mbadziong.stooq.stooq.report.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import com.mbadziong.stooq.stooq.report.ReportWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Optional;

@Service
public class ReportService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportService.class);

    private ReportWriter reportWriter;

    private List<MarketIndex> latestReading;

    public ReportService(ReportWriter reportWriter) {
        this.reportWriter = reportWriter;
    }

    public List<MarketIndex> handleNewMarketReading(List<MarketIndex> current) {
        if(current.equals(latestReading) || current.contains(null)) {
            LOGGER.info("Skipping append to file, current values are the same as latest or empty.");
            return null;
        } else {
            try {
                reportWriter.processAndWriteToFile(current);
                latestReading = current;
                return current;
            } catch (JsonProcessingException e) {
                LOGGER.error("Error while processing data.", e.getMessage());
            }
            return null;
        }
    }
}

