package com.mbadziong.stooq.stooq.report.model;

import com.mbadziong.stooq.stooq.data.model.MarketIndex;

import java.time.LocalDateTime;
import java.util.List;

public class ReportRow {

    private List<MarketIndex> marketIndexesList;

    public ReportRow() {
    }

    public ReportRow(List<MarketIndex> marketIndexesList) {
        this.marketIndexesList = marketIndexesList;
    }

    public List<MarketIndex> getMarketIndexesList() {
        return marketIndexesList;
    }

    public void setMarketIndexesList(List<MarketIndex> marketIndexesList) {
        this.marketIndexesList = marketIndexesList;
    }

    @Override
    public String toString() {
        return "ReportRow{" +
                "marketIndexesList=" + marketIndexesList +
                '}';
    }
}
