package com.mbadziong.stooq.stooq.report;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.mbadziong.stooq.stooq.data.model.MarketIndex;
import com.mbadziong.stooq.stooq.report.model.ReportRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ReportWriter {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportWriter.class);

    @Value("${report_path}")
    private String reportPath;

    @Autowired
    private ObjectMapper mapper;

    public void processAndWriteToFile(List<MarketIndex> marketIndexesList) throws JsonProcessingException {
        ReportRow row = new ReportRow(marketIndexesList);
        try {
            List<ReportRow> currentRows = getCurrentReport();
            currentRows.add(row);
            List<String> lines = Arrays.asList(mapper.writeValueAsString(currentRows));
            writeToFile(lines);

        } catch (IOException e) {
            LOGGER.error("Error gaining current data.", e.getMessage());
        }
    }

    private void writeToFile(List<String> lines) {
        try {
            Files.write(
                    Paths.get(reportPath),
                    lines,
                    Charset.defaultCharset(),
                    StandardOpenOption.WRITE,
                    StandardOpenOption.CREATE
            );
        } catch (IOException e) {
            LOGGER.error("Error while appending new row to file.", e.getMessage());
        }
    }

    private List<ReportRow> getCurrentReport() throws IOException {
        List<String> content = new ArrayList<>();
        List<ReportRow> currentRows = new ArrayList<>();
        try {
            content = Files.readAllLines(Paths.get(reportPath));
        } catch (IOException e) {
            LOGGER.error("Error while reading file.");
        }

        String json = String.join("", content);
        CollectionType typeReference = TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, ReportRow.class);
        if (!StringUtils.isEmpty(json)) {
            currentRows = mapper.readValue(json, typeReference);
        }

        return currentRows;
    }
}